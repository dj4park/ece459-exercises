// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n == 0 {
        return 0;
    }

    if n == 1 {
        return 1;
    }

    let mut fib_numbers = vec![0, 1];
    let n_usize: usize = n as usize;

    for i in 2..=n_usize {
        let new_fib = fib_numbers[i-1] + fib_numbers[i-2];
        fib_numbers.push(new_fib);
    }

    return fib_numbers[n_usize];
}


fn main() {
    println!("{}", fibonacci_number(10));
}
